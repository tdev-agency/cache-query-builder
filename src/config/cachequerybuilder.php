<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Cache lifetime
    |--------------------------------------------------------------------------
    |
    | Set cache lifetime. If set -1 will store forever
    |
    */

    'lifetime' => env('CQB_LIFETIME', 1),

    'prefix' => env('CQB_CACHE_PREFIX', 'cqb_'),

    'driver' => env('CQB_CACHE_DRIVER', env('CACHE_DRIVER')),

    'invalidate' => env('CQB_CACHE_INVALIDATE', false)

];
