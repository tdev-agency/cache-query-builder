<?php

namespace TrushDev\CacheQueryBuilder;

use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\Cache;

class Builder extends QueryBuilder
{
    private $model_name;

    private $lifetime;

    /**
     */
    public function setModelName($model_name)
    {
        $this->model_name = $model_name;
        return $this;
    }

    public function setLifeTime($lifeTime)
    {
        $this->lifetime = $lifeTime;
    }

    /**
     * @return \DateTime|int
     * @throws \Exception
     */
    protected function getLifeTime()
    {
        if ($this->lifetime !== null) {
            return $this->lifetime;
        }
        
        return (int)config('cachequerybuilder.lifetime') === -1 ? 365 * 24 * 60 : (int)config('cachequerybuilder.lifetime');
    }

    /**
     * Run the query as a "select" statement against the connection.
     *
     * @return array
     * @throws \Exception
     */
    protected function runSelect()
    {
        $cacheKey = sprintf('%s%s',
            config('cachequerybuilder.prefix'),
            md5(serialize(array_merge([$this->toSql()], $this->getBindings())))
        );
        $cache = Cache::store('cqb_cache');
        if (method_exists($cache->getStore(), 'tags')) {
            $cache = $cache->tags([$this->model_name]);
        }
        return $cache->remember($cacheKey, $this->getLifeTime(), function () {
            return parent::runSelect();
        });
    }
}
