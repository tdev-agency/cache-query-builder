<?php

namespace TrushDev\CacheQueryBuilder;

use Illuminate\Database\Connection;

trait CacheQueryBuilder
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }

    /**
     * Get a new query builder instance for the connection.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        /** @var Connection $conn */
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();


        $builder = new Builder($conn, $grammar, $conn->getPostProcessor());
        $builder->setModelName(self::class);

        if (isset($this->cqb_lifetime) && $this->cqb_lifetime !== null) {
            $builder->setLifeTime($this->cqb_lifetime);
        }

        return $builder;
    }

}
