<?php

namespace TrushDev\CacheQueryBuilder;

use Illuminate\Support\ServiceProvider;

class CacheQueryBuilderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'cachequerybuilder.php' => config_path('cachequerybuilder.php'),
        ], 'config');

        $this->app->make('Illuminate\Contracts\Http\Kernel')->pushMiddleware(
            CacheQueryBuilderMiddleware::class
        );
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'cachequerybuilder.php', 'cachequerybuilder'
        );

        $this->app['config']->set('cache.stores.cqb_cache', [
            'driver' => $this->app['config']->get('cachequerybuilder.driver', 'array'),
            'path' => storage_path('framework/cache/data/cqb_cache')

        ]);
    }
}
