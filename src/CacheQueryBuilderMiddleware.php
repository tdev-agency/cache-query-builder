<?php

namespace TrushDev\CacheQueryBuilder;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class CacheQueryBuilderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        if((bool)Config::get('cachequerybuilder.invalidate') === true) {
            Cache::store('cqb_cache')->clear();
        }
    }
}
