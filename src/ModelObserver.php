<?php

namespace TrushDev\CacheQueryBuilder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class ModelObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param Model $model
     * @return void
     */
    public function created(Model $model)
    {
        $this->cacheClear($model);
    }

    private function cacheClear(Model $model)
    {
        $cache = Cache::store('cqb_cache');
        if(method_exists($cache->getStore(), 'tags')) {
            $reflection = new \ReflectionClass($model);
            $tags = [$reflection->getName()];
            $relations = $model->getRelations();
            if(!empty($relations)) {
                foreach($relations as $relation) {
                    $reflection = new \ReflectionClass($relation);
                    $tags[] = $reflection->getName();
                }
            }
            $cache->tags($tags)->flush();
        } else {
            $cache->clear();
        }
    }

    /**
     * Handle the User "saved" event.
     *
     * @param Model $model
     * @return void
     */
    public function saved(Model $model)
    {
        $this->cacheClear($model);
    }

    /**
     * Handle the User "updated" event.
     *
     * @param Model $model
     * @return void
     */
    public function updated(Model $model)
    {
        $this->cacheClear($model);
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param Model $model
     * @return void
     */
    public function deleted(Model $model)
    {
        $this->cacheClear($model);
    }
}
